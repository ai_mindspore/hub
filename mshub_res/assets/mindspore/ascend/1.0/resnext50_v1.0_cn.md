# ResNeXt-50

---

模型名称：ResNeXt-50

骨干网络：ResNeXt-50

模块类型：cv-classification

可微调：True

输入形状： [224, 224, 3]

模型版本：1.0

作者：MindSpore团队

更新时间：2020-9-22

代码仓链接： <https://gitee.com/mindspore/mindspore/tree/r1.0/model_zoo/official/cv/resnext50>

用户ID：MindSpore

用途：推理

训练后端：Ascend

推理后端：Ascend

MindSpore版本：1.0

资源:

-
    文件格式: ckpt  
    资源链接: <https://download.mindspore.cn/model_zoo/r1.1/resnext50_ascend_v111_imagenet2012_offical_cv_bs128_acc78/resnext50_ascend_v111_imagenet2012_offical_cv_bs128_acc78.ckpt>
    资源SHA256校验码: 581d3e7a97d408d4f690ef5f362e5435d853348f1f0f18c351931ab77e8f3e45

许可证：Apache 2.0

摘要：使用ResNeXt-50进行图片分类。

---

## 简介

该MindSpore Hub模型使用码云上MindSpore ModelZoo中的ResNeXt50实现，目录为model_zoo/official/cv/resnext50。

更多详情参见[码云MindSpore ModelZoo](https://gitee.com/mindspore/mindspore/blob/r1.0/model_zoo/official/cv/resnext50/README.md)

## 参考论文

1. Xie S, Girshick R, Dollár, Piotr, et al. Aggregated Residual Transformations for Deep Neural Networks. 2016.
